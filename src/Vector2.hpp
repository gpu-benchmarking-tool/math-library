/*__________________________________________________
 |                                                  |
 |    File: Vector2.hpp                             |
 |    Author: Nabil Sadeg                           |
 |                                                  |
 |    Description: 2D Vector class.                 |
 |_________________________________________________*/



#pragma once

#include <cmath>

namespace math {


/**
 * @brief      2D Vector class containing 2D vector operations.
 * Operations available: Vec+Vec, Vec+=Vec, Vec-Vec, Vec-=Vec,
 * Vec=Vec, Vec==Vec, Vec*d, Vec*=d, Vec/d, Vec/=d, Vec.doc(Vec),
 * Vec.normal(), Vec.normalise(), Vec.magnitude(), Vec.magnitudeSquare()
 */
class Vector2 {

  public:

    double x, y;


    /**
     * @brief      Constructs the object.
     * Empty construcor sets x=0 and y=0.
     */
    Vector2() : x(0), y(0) {}


    /**
     * @brief      Constructs the object.
     * Uses the x and y provided to build the 2D vector.
     *
     * @param[in]  x     x component of the 2D vector
     * @param[in]  y     y component of the 2D vector
     */
    Vector2(double x, double y) : x(x), y(y) {}


    /**
     * @brief      Destroys the object.
     */
    ~Vector2() {}


    /**
     * @brief      Overloads + operator.
     * Vec+Vec, sum of the x and y components of the 2D vector.
     *
     * @param      other  The other vector used in the operation
     *
     * @return     Result of the addition.
     */
    Vector2 operator +(const Vector2 &other) {
        return Vector2(x + other.x, y + other.y);
    }


    /**
     * @brief      Overloads += operator.
     * Vec+=Vec, sum of the x and y components.
     *
     * @param      other  The other vector used in the operation
     */
    void operator +=(const Vector2 &other) {
        x += other.x;
        y += other.y;
    }


    /**
     * @brief      Overloads - operator.
     * Vec-Vec, subtraction of x and y components.
     *
     * @param      other  The other vector used in the operation
     *
     * @return     Result of the subtraction.
     */
    Vector2 operator -(const Vector2 &other) {
        return Vector2(x - other.x, y - other.y);
    }


    /**
     * @brief      Overloads -= operator.
     * Vec-=Vec, subtraction of x and y components.
     *
     * @param      other  The other vector used in the operation
     */
    void operator -=(const Vector2 &other) {
        x -= other.x;
        y -= other.y;
    }


    /**
     * @brief      Overloads = operator.
     * Vec=Vec, assign x and y components to left hand side 2D vector.
     *
     * @param[in]  other  The other vector used in the operation
     */
    void operator =(const Vector2 &other) {
        x = other.x;
        y = other.y;
    }


    /**
     * @brief      Overloads == operator.
     * Vec==Vec, compares if x and y components are the same.
     *
     * @param      other  The other vector used in the operation
     *
     * @return     True if vectors are the same, False otherwise.
     */
    bool operator ==(const Vector2 &other) {
        return x == other.x && y == other.y;
    }


    /**
     * @brief      Overloads * operator.
     * Vec*Vec, multiplies the x and y components of the 2D vectors.
     *
     * @param[in]  value  The double value used in the operation
     *
     * @return     Result of the multiplication.
     */
    Vector2 operator *(double value) {
        return Vector2(x * value, y * value);
    }


    /**
     * @brief      Overloads *= operator.
     * Vec*=Vec, multiplies the x and y components of the 2D vectors.
     *
     * @param[in]  value  The double value used in the operation
     */
    void operator *=(double value) {
        x *= value;
        y *= value;
    }


    /**
     * @brief      Overloads / operator.
     * Vec/Vec, divide left vector by right vector.
     *
     * @param[in]  value  The double value used in the operation
     *
     * @return     Result of the division.
     */
    Vector2 operator /(double value) {
        return Vector2(x / value, y / value);
    }


    /**
     * @brief      Overloads /= operator.
     * Vec/=Vec, divide left vector by right vector.
     *
     * @param[in]  value  The double value used in the operation
     */
    void operator /=(double value) {
        x /= value;
        y /= value;
    }


    /**
     * @brief      Dot product.
     * Vec.dot(Vec), result of the dot product operation.
     * V1 is first vector and V2 second vector,
     * DotProductResult = V1.x * V2.x + V1.y * V2.y.
     *
     * @param      other  The other vector used in the operation
     *
     * @return     Result of the dot product.
     */
    double dot(Vector2 &other) {
        return x * other.x + y * other.y;
    }


    /**
     * @brief      Computes the vector mormal.
     * Vec.normal(), divides x and y components by magnitude.
     *
     * @return     Normal 2D vector.
     */
    Vector2 normal() {
        double m = magnitude();
        return Vector2(x / m, y / m);
    }


    /**
     * @brief      Normalises the vector.
     * Vec.normal(), divides x and y components by magnitude.
     */
    void normalise() {
        double m = magnitude();
        x /= m;
        y /= m;
    }


    /**
     * @brief      Computes the magnitude.
     * Magnitude = sqrt(x*x + y*y).
     *
     * @return     The vector magnitude.
     */
    double magnitude() {
        return std::sqrt(x * x + y * y);
    }


    /**
     * @brief      Computes the magnitude squared.
     * MagnitudeSquared = x*x + y*y.
     *
     * @return     The magnitude squared.
     */
    double magnitudeSquared() {
        return x * x + y * y;
    }
};
}