/*__________________________________________________
 |                                                  |
 |  File: Vector3.hpp                               |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Struct representing a 3D Vector.   |
 |_________________________________________________*/



#pragma once


#include <cmath>


namespace math {


/**
 * @brief      Vector3 structure.
 * Operations available: Vec+Vec, Vec+=Vec, Vec-Vec, Vec-=Vec, Vec=Vec,
 * Vec==Vec, Vec*d, Vec*=d, Vec/d, Vec/=d, Vec.doc(Vec), Vec.cross(Vec),
 * Vec.normal(), Vec.normalise(), Vec.magnitude(), Vec.magnitudeSquare()
 */
class Vector3 {

  public:

    double x, y, z;


    /**
     * Default constructor.
     * Empty construcor sets x=0, y=0 and z=0.
     */
    Vector3() : x(0), y(0), z(0) {}


    /**
     * @brief      Constructor.
     * Uses the x, y and z provided to build the 3D vector.
     *
     * @param[in]  x     x value
     * @param[in]  y     y value
     * @param[in]  z     z value
     */
    Vector3(double x, double y, double z) : x(x), y(y), z(z) {}


    /**
     * @brief      Destroys the object.
     */
    ~Vector3() {}


    /**
     * @brief      Overloads + operator.
     * Vec+Vec, sum of the x, y and z components of the 3D vector.
     *
     * @param[in]  other  The other Vector3 used in the operation
     *
     * @return     Sum of the two vectors.
     */
    Vector3 operator +(const Vector3 &other) {
        return Vector3(x + other.x, y + other.y, z + other.z);
    }


    /**
     * @brief      Overloads += operator.
     * Vec+=Vec, sum of the x, y and z components.
     *
     * @param[in]  other  The other Vector3 used in the operation
     */
    void operator +=(const Vector3 &other) {
        x += other.x;
        y += other.y;
        z += other.z;
    }


    /**
     * @brief      Overloads - operator.
     * Vec-Vec, subtraction of x, y and z components.
     *
     * @param[in]  other  The other Vector3 used in the operation
     *
     * @return     Substraction of the two vectors.
     */
    Vector3 operator -(const Vector3 &other) {
        return Vector3(x - other.x, y - other.y, z - other.z);
    }


    /**
     * @brief      Overloads -= operator.
     * Vec-=Vec, subtraction of x, y and z components.
     *
     * @param[in]  other  The other Vector3 used in the operation
     */
    void operator -=(const Vector3 &other) {
        x -= other.x;
        y -= other.y;
        z -= other.z;
    }


    /**
     * @brief      Overloads equal operator.
     * Vec=Vec, assign x, y and z components to left hand side 3D vector.
     *
     * @param[in]  other  The other Vector3 used in the operation
     */
    void operator =(const Vector3 &other) {
        x = other.x;
        y = other.y;
        z = other.z;
    }


    /**
     * @brief      Overloads == operator.
     * Vec==Vec, compares if x, y and z components are the same.
     *
     * @param      other  The other vector
     *
     * @return     True if vectors are the same, False otherwise.
     */
    bool operator ==(const Vector3 &other) {
        return x == other.x && y == other.y && z == other.z;
    }


    /**
     * @brief      Overloads multiplication with double.
     * Vec*Vec, multiplies the x, y and z components of the 3D vectors.
     *
     * @param[in]  other  Double value used in the operation
     *
     * @return     Multiplication result.
     */
    Vector3 operator *(double other) {
        return Vector3(other * x, other * y, other * z);
    }


    /**
     * @brief      Overloads *= operator.
     * Vec*=Vec, multiplies the x, y and z components of the 3D vectors.
     *
     * @param[in]  other  Double value used in the operation
     */
    void operator *=(double other) {
        x *= other;
        y *= other;
        z *= other;
    }


    /**
     * @brief      Overloads division with double.
     * Vec/Vec, divide left vector by right vector.
     *
     * @param[in]  other  Double value used in the operation
     *
     * @return     Division result.
     */
    Vector3 operator /(double other) {
        return Vector3(x / other, y / other, z / other);
    }


    /**
     * @brief      Overloads /= operator.
     * Vec/=Vec, divide left vector by right vector.
     *
     * @param[in]  other  Double value used in the operation
     */
    void operator /=(double other) {
        x /= other;
        y /= other;
        z /= other;
    }


    /**
     * @brief      Dot product.
     * Vec.dot(Vec), result of the dot product operation.
     * V1 is first vector and V2 second vector,
     * DotProductResult = V1.x * V2.x + V1.y * V2.y + V1.z * V2.z.
     *
     * @param      other  The other vector
     *
     * @return     Result of the dot product.
     */
    double dot(const Vector3 &other) {
        return x * other.x + y * other.y + z * other.z;
    }


    /**
     * @brief      Cross product.
     * V1 is the first vector and V2 is the second vector,
     * CrossProduct = [V1.y*V2.z - V1.z*V2.y, V1.z*V2.x - V1.x*V2.z, V1.x*V2.y-V1.y*V2.x].
     *
     * @param[in]  other  The other Vector3 used in the operation
     *
     * @return     Result of the cross product.
     */
    Vector3 cross(const Vector3 &other) {
        return Vector3(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
    }


    /**
     * @brief      Computes the vector mormal.
     * Vec.normal(), divides x, y and z components by magnitude.
     *
     * @return     Normal 3D vector.
    */
    Vector3 normal() {
        double m = magnitude();
        if (m == 0) return Vector3(x, y, z);
        return Vector3(x / m, y / m, z / m);
    }


    /**
     * @brief      Normalises the vector.
     * Vec.normal(), divides x, y and z components by magnitude.
     */
    void normalise() {

        double m = magnitude();

        if (m == 0) return;

        x /= m;
        y /= m;
        z /= m;
    }


    /**
     * @brief      Computes the vector magnitude.
     * Magnitude = sqrt(x*x + y*y + z*z).
     *
     * @return     Magnitude computed.
     */
    double magnitude() {
        return std::sqrt(x * x + y * y + z * z);
    }


    /**
     * @brief      Computes the magnitude squared.
     * MagnitudeSquared = x*x + y*y + z*z.
     *
     * @return     Magnitude squared.
     */
    double magnitudeSquared() {
        return x * x + y * y + z * z;
    }
};
}