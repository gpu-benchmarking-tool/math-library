/*__________________________________________________
 |                                                  |
 |  File: MathLibrary.hpp                           |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Main header.                       |
 |_________________________________________________*/



#pragma once

#include "Vector2.hpp"
#include "Vector3.hpp"
