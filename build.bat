git submodule update --init --recursive
git submodule update --remote --recursive

rmdir /s /Q build
mkdir build
cd build
cmake .. -G "MinGW Makefiles"
cmake --build . --config Release