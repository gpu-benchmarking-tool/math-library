# Math Library

*This repository is part of the [**GPU Benchmarking Tool**](https://gpu-benchmarking-documentation.firebaseapp.com/) project.*  

This component is a C++ library used to do the Math computations.

## Compile and run (Windows, Linux and macOS)
- Make sure you have *git* installed
    - [Git for Windows](https://git-scm.com/download/win)
    - [Git for Linux / macOS](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Make sure *cmake* is installed
    - [Cmake website](https://cmake.org/install/)
- On Windows only, you need to install *MinGW* and add it to the path
    - [MinGW website](http://www.mingw.org/wiki/Install_MinGW)
- This is a header only library, so you simply need to include the main header `src/MathLibrary.hpp`
- To build the tests, run `build.bat` on Windows or `build.sh` on Linux / macOS
    - On Windows, the test will be located at `builds\test\tests.exe`
    - On Linux and macOS, the tests will be in `bin`


## Structure of the repository
- `assets` contains the repository icon
- `doc` contains the documentation
- `models` contains the cube used for the tests
- `src` contains the source code
- `test` contains the test code
- `.gitlab-ci.yml` contains the Continuous Integration configuration
- `build.bat` and `build.sh` are used to compile the project
- `Doxygen` contains the odumentation generation configuration
- `generateDocumentation.bat` and `generateDocumentation.sh` are used to generate the documentation

## Generate the documentation
- To generate the documentation, make sure you have [Doxygen](http://www.doxygen.nl/manual/install.html) installed as well as [LaTeX](https://www.latex-project.org/get/)
- Run `generateDocumentation.bat` on Windows or `generateDocumentation.sh` on Linux / macOS to generate the pdf documentation
- The documentation will be generated in the `doc` folder