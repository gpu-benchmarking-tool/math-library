/*__________________________________________________
 |                                                  |
 |  File: Vector2Tests.cpp                          |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Vector2 Unit tests.                |
 |_________________________________________________*/



#include "gtest/gtest.h"
#include "MathLibrary.hpp"


TEST(Vector2, DefaultConstructor) {
    math::Vector2 v;
    EXPECT_EQ(v.x, 0.);
    EXPECT_EQ(v.y, 0.);
}

TEST(Vector2, Constructor) {
    math::Vector2 v(15.4, -54.1);
    EXPECT_EQ(v.x, 15.4);
    EXPECT_EQ(v.y, -54.1);
}

TEST(Vector2, Vector2_Plus_Vector2) {
    math::Vector2 v1(54.9, 10);
    math::Vector2 v2(20, -65.8);
    math::Vector2 v3 = v1 + v2;
    EXPECT_EQ(v1.x, 54.9);
    EXPECT_EQ(v1.y, 10.);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
    EXPECT_EQ(v3.x, 54.9 + 20);
    EXPECT_EQ(v3.y, 10 - 65.8);
}

TEST(Vector2, Vector2_PlusEqual_Vector2) {
    math::Vector2 v1(54.9, 10);
    math::Vector2 v2(20, -65.8);
    v1 += v2;
    EXPECT_EQ(v1.x, 54.9 + 20);
    EXPECT_EQ(v1.y, 10 - 65.8);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
}

TEST(Vector2, Vector2_Minus_Vector2) {
    math::Vector2 v1(54.9, 10);
    math::Vector2 v2(20, -65.8);
    math::Vector2 v3 = v1 - v2;
    EXPECT_EQ(v1.x, 54.9);
    EXPECT_EQ(v1.y, 10.);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
    EXPECT_EQ(v3.x, 54.9 - 20);
    EXPECT_EQ(v3.y, 10 + 65.8);
}

TEST(Vector2, Vector2_MinusEqual_Vector2) {
    math::Vector2 v1(54.9, 10);
    math::Vector2 v2(20, -65.8);
    v1 -= v2;
    EXPECT_EQ(v1.x, 54.9 - 20);
    EXPECT_EQ(v1.y, 10 + 65.8);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
}

TEST(Vector2, Vector2_Assign_Vector2) {
    math::Vector2 v1(54.9, 10);
    math::Vector2 v2(20, -65.8);
    v1 = v2;
    EXPECT_EQ(v1.x, 20.);
    EXPECT_EQ(v1.y, -65.8);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
}

TEST(Vector2, Vector2_Equal_Vector2) {
    math::Vector2 v1(1, 2);
    math::Vector2 v2(1, 3);
    EXPECT_EQ(v1.x, v2.x);
    EXPECT_NE(v1.y, v2.y);
    EXPECT_EQ(v1 == v2, false);

    v2.y = 2;
    EXPECT_EQ(v1.x, v2.x);
    EXPECT_EQ(v1.y, v2.y);
    EXPECT_EQ(v1 == v2, true);
}

TEST(Vector2, Vector2_Times_Value) {
    math::Vector2 v1(1, 2);
    math::Vector2 v2 = v1 * 5;

    EXPECT_EQ(v2.x, 5.);
    EXPECT_EQ(v2.y, 10.);

    v2 = v2 * -1;
    EXPECT_EQ(v2.x, -5.);
    EXPECT_EQ(v2.y, -10.);
}

TEST(Vector2, Vector2_TimesEqual_Value) {
    math::Vector2 v(1, 2);
    v *= 5;

    EXPECT_EQ(v.x, 5.);
    EXPECT_EQ(v.y, 10.);

    v *= -1;
    EXPECT_EQ(v.x, -5.);
    EXPECT_EQ(v.y, -10.);
}

TEST(Vector2, Vector2_DividedBy_Value) {
    math::Vector2 v1(1, 2);
    math::Vector2 v2 = v1 / 2;

    EXPECT_EQ(v2.x, .5);
    EXPECT_EQ(v2.y, 1.);
}

TEST(Vector2, Vector2_DividedByEqual_Value) {
    math::Vector2 v(1, 2);
    v /= 2;

    EXPECT_EQ(v.x, .5);
    EXPECT_EQ(v.y, 1.);
}

TEST(Vector2, DotProduct) {
    math::Vector2 v1(2, 5);
    math::Vector2 v2(15, -.4);

    double d = v1.dot(v2);

    EXPECT_EQ(d, 28.);
}

TEST(Vector2, Normal) {
    math::Vector2 v1(2, 5);
    math::Vector2 v2 = v1.normal();

    EXPECT_EQ(v2.x, 2 / std::sqrt(29));
    EXPECT_EQ(v2.y, 5 / std::sqrt(29));

    math::Vector2 v3(0, 0);
    v3.normal();
}

TEST(Vector2, Normalise) {
    math::Vector2 v(2, 5);
    v.normalise();

    EXPECT_EQ(v.x, 2 / std::sqrt(29));
    EXPECT_EQ(v.y, 5 / std::sqrt(29));

    math::Vector2 v1(0, 0);
    v1.normalise();
}

TEST(Vector2, Magnitude) {
    math::Vector2 v(2, 5);
    EXPECT_EQ(v.magnitude(), std::sqrt(29));
}

TEST(Vector2, MagnitudeSquared) {
    math::Vector2 v(2, 5);
    EXPECT_EQ(v.magnitudeSquared(), 29.);
}