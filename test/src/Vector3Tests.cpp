/*__________________________________________________
 |                                                  |
 |  File: Vector3Tests.cpp                          |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Vector3 Unit tests.                |
 |_________________________________________________*/



#include "gtest/gtest.h"
#include "MathLibrary.hpp"


TEST(Vector3, DefaultConstructor) {
    math::Vector3 v;
    EXPECT_EQ(v.x, 0.);
    EXPECT_EQ(v.y, 0.);
    EXPECT_EQ(v.z, 0.);
}

TEST(Vector3, Constructor) {
    math::Vector3 v(15.4, -54.1, 14.2);
    EXPECT_EQ(v.x, 15.4);
    EXPECT_EQ(v.y, -54.1);
    EXPECT_EQ(v.z, 14.2);
}

TEST(Vector3, Vector3_Plus_Vector3) {
    math::Vector3 v1(54.9, 10, 1);
    math::Vector3 v2(20, -65.8, 15);
    math::Vector3 v3 = v1 + v2;
    EXPECT_EQ(v1.x, 54.9);
    EXPECT_EQ(v1.y, 10.);
    EXPECT_EQ(v1.z, 1.);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
    EXPECT_EQ(v2.z, 15.);
    EXPECT_EQ(v3.x, 54.9 + 20);
    EXPECT_EQ(v3.y, 10 - 65.8);
    EXPECT_EQ(v3.z, 1 + 15.);
}

TEST(Vector3, Vector3_PlusEqual_Vector3) {
    math::Vector3 v1(54.9, 10, 1);
    math::Vector3 v2(20, -65.8, 15);
    v1 += v2;
    EXPECT_EQ(v1.x, 54.9 + 20);
    EXPECT_EQ(v1.y, 10 - 65.8);
    EXPECT_EQ(v1.z, 1 + 15.);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
    EXPECT_EQ(v2.z, 15.);
}

TEST(Vector3, Vector3_Minus_Vector3) {
    math::Vector3 v1(54.9, 10, 1);
    math::Vector3 v2(20, -65.8, 15);
    math::Vector3 v3 = v1 - v2;
    EXPECT_EQ(v1.x, 54.9);
    EXPECT_EQ(v1.y, 10.);
    EXPECT_EQ(v1.z, 1.);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
    EXPECT_EQ(v2.z, 15.);
    EXPECT_EQ(v3.x, 54.9 - 20);
    EXPECT_EQ(v3.y, 10 + 65.8);
    EXPECT_EQ(v3.z, 1 - 15.);
}

TEST(Vector3, Vector3_MinusEqual_Vector3) {
    math::Vector3 v1(54.9, 10, 1);
    math::Vector3 v2(20, -65.8, 15);
    v1 -= v2;
    EXPECT_EQ(v1.x, 54.9 - 20);
    EXPECT_EQ(v1.y, 10 + 65.8);
    EXPECT_EQ(v1.z, 1 - 15.);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
    EXPECT_EQ(v2.z, 15.);
}

TEST(Vector3, Vector3_Assign_Vector3) {
    math::Vector3 v1(54.9, 10, 1);
    math::Vector3 v2(20, -65.8, 15);
    v1 = v2;
    EXPECT_EQ(v1.x, 20.);
    EXPECT_EQ(v1.y, -65.8);
    EXPECT_EQ(v1.z, 15.);
    EXPECT_EQ(v2.x, 20.);
    EXPECT_EQ(v2.y, -65.8);
    EXPECT_EQ(v2.z, 15.);
}

TEST(Vector3, Vector3_Equal_Vector3) {
    math::Vector3 v1(1, 2, 4);
    math::Vector3 v2(1, 3, 4);
    EXPECT_EQ(v1.x, v2.x);
    EXPECT_NE(v1.y, v2.y);
    EXPECT_EQ(v1.z, v2.z);
    EXPECT_EQ(v1 == v2, false);

    v2.y = 2;
    EXPECT_EQ(v1.x, v2.x);
    EXPECT_EQ(v1.y, v2.y);
    EXPECT_EQ(v1.z, v2.z);
    EXPECT_EQ(v1 == v2, true);
}

TEST(Vector3, Vector3_Times_Value) {
    math::Vector3 v1(1, 2, 3);
    math::Vector3 v2 = v1 * 5;

    EXPECT_EQ(v2.x, 5.);
    EXPECT_EQ(v2.y, 10.);
    EXPECT_EQ(v2.z, 15.);

    v2 = v2 * -1;
    EXPECT_EQ(v2.x, -5.);
    EXPECT_EQ(v2.y, -10.);
    EXPECT_EQ(v2.z, -15.);
}

TEST(Vector3, Vector3_TimesEqual_Value) {
    math::Vector3 v(1, 2, 3);
    v *= 5;

    EXPECT_EQ(v.x, 5.);
    EXPECT_EQ(v.y, 10.);
    EXPECT_EQ(v.z, 15.);

    v *= -1;
    EXPECT_EQ(v.x, -5.);
    EXPECT_EQ(v.y, -10.);
    EXPECT_EQ(v.z, -15.);
}

TEST(Vector3, Vector3_DividedBy_Value) {
    math::Vector3 v1(1, 2, 4);
    math::Vector3 v2 = v1 / 2;

    EXPECT_EQ(v2.x, .5);
    EXPECT_EQ(v2.y, 1.);
    EXPECT_EQ(v2.z, 2.);
}

TEST(Vector3, Vector3_DividedByEqual_Value) {
    math::Vector3 v(1, 2, 4);
    v /= 2;

    EXPECT_EQ(v.x, .5);
    EXPECT_EQ(v.y, 1.);
    EXPECT_EQ(v.z, 2.);
}

TEST(Vector3, DotProduct) {
    math::Vector3 v1(2, 5, 4);
    math::Vector3 v2(15, -.4, .8);

    double d = v1.dot(v2);

    EXPECT_EQ(d, 31.2);
}

TEST(Vector3, CrossProduct) {
    math::Vector3 v1(2, 5, 4);
    math::Vector3 v2(15, -.4, .8);

    math::Vector3 v3 = v1.cross(v2);

    EXPECT_EQ(v3.x, 5.6);
    EXPECT_EQ(v3.y, 58.4);
    EXPECT_EQ(v3.z, -75.8);
}

TEST(Vector3, Normal) {
    math::Vector3 v1(2, 5, 3);
    math::Vector3 v2 = v1.normal();

    EXPECT_EQ(v2.x, 2 / std::sqrt(38));
    EXPECT_EQ(v2.y, 5 / std::sqrt(38));
    EXPECT_EQ(v2.z, 3 / std::sqrt(38));

    math::Vector3 v3(0, 0, 0);
    v3.normal();
}

TEST(Vector3, Normalise) {
    math::Vector3 v(2, 5, 3);
    v.normalise();

    EXPECT_EQ(v.x, 2 / std::sqrt(38));
    EXPECT_EQ(v.y, 5 / std::sqrt(38));
    EXPECT_EQ(v.z, 3 / std::sqrt(38));

    math::Vector3 v1(0, 0, 0);
    v1.normalise();
}

TEST(Vector3, Magnitude) {
    math::Vector3 v(2, 5, 3);
    EXPECT_EQ(v.magnitude(), std::sqrt(38));
}

TEST(Vector3, MagnitudeSquared) {
    math::Vector3 v(2, 5, 3);
    EXPECT_EQ(v.magnitudeSquared(), 38.);
}